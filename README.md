# Online Bookstore API

## Description:
This project is a RESTful API for an online bookstore designed using Django and Django REST framework. The API not only supports the CRUD operations for books, authors, and categories but also allows user registration and authentication. The users have the added advantage of adding books to a shopping cart. Once the purchase of books is successful, an email notification is dispatched to the user.

## Features:
- User Registration & Authentication
- CRUD operations for Books, Authors, and Categories
- Shopping Cart functionality
- Email notifications upon successful purchase via Celery
- Docker support for containerized development
- CI/CD via GitHub Actions

## Local Setup & Development:

**Prerequisites:**
- Docker
- Python 3.8+
- pip

### Steps:
1. **Clone the Repository:**
    ```bash
    git clone https://gitlab.com/Ahmad.Aftab/book-store-api.git
    ```

2. **Navigate to Project Directory:**
    ```bash
    cd [Your Repository Name]
    ```

3. **Install Dependencies:**
    ```bash
    pip install -r requirements.txt
    ```

4. **Database Migrations:**
    ```bash
    python manage.py migrate
    ```

5. **Run the Development Server:**
    ```bash
    python manage.py runserver
    ```

6. The application will be accessible at `http://127.0.0.1:8000/`.

## API Endpoints:

- **Users**:
    - Register: `POST /api/register/`
    - Login: `POST /api/login/`
    - Logout: `POST /api/logout/`

- **Books**:
    - List & Create: `GET, POST /api/books/`
    - Retrieve, Update & Delete: `GET, PUT, DELETE /api/books/<id>/`

- **Authors**:
    - List & Create: `GET, POST /api/authors/`
    - Retrieve, Update & Delete: `GET, PUT, DELETE /api/authors/<id>/`

- **Categories**:
    - List & Create: `GET, POST /api/categories/`
    - Retrieve, Update & Delete: `GET, PUT, DELETE /api/categories/<id>/`

Detailed information on request/response formats can be found at the OpenAPI/Swagger documentation at `http://127.0.0.1:8000/swagger/`.

## Docker:

**Docker Setup:**

1. **Build the Docker Image:**
    ```bash
    docker-compose build
    ```

2. **Run the Docker Container:**
    ```bash
    docker-compose up
    ```

For more advanced Docker commands and configurations, please refer to the official [Docker documentation](https://docs.docker.com/).

## CI/CD with GitHub Actions:

The repository integrates GitHub Actions for continuous testing and deployment. Workflows are defined in the `.github/workflows/` directory. On every push to the `main` branch, these workflows are triggered. They are responsible for running tests and deploying the application if required.

For further understanding of each CI/CD step, kindly refer to the workflow files in the `.github/workflows/` directory.
