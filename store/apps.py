from django.contrib import admin
from .models import Book, Author, Category, ShoppingCart

@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ['title', 'author', 'published_date', 'ISBN', 'category']
    search_fields = ['title', 'author__name', 'ISBN']
    list_filter = ['published_date', 'category']

@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']

@admin.register(ShoppingCart)
class ShoppingCartAdmin(admin.ModelAdmin):
    list_display = ['user', 'purchased']
    search_fields = ['user__username']
    list_filter = ['purchased']
