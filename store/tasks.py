from celery import shared_task
from django.core.mail import send_mail

@shared_task
def send_purchase_email(user_email, purchase_details):
    subject = "Purchase Successful"
    message = f"Hello, you've successfully purchased the following items:\n\n{purchase_details}"
    email_from = 'noreply@bookstore.com'  # Change this to your sender email
    recipient_list = [user_email]
    send_mail(subject, message, email_from, recipient_list)
