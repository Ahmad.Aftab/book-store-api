from rest_framework.test import APITestCase
from rest_framework import status
from django.urls import reverse
from django.contrib.auth.models import User
from store.models import Book, Author, Category
from store.serializers import BookSerializer, AuthorSerializer, CategorySerializer

class UserRegistrationTestCase(APITestCase):

    def test_successful_registration(self):
        data = {"username": "testuser", "password": "secret123"}
        response = self.client.post(reverse('user-register'), data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)

class AuthorAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='testpass')
        self.client.login(username='testuser', password='testpass')
        self.author = Author.objects.create(name="J.K. Rowling")

    def test_list_authors(self):
        response = self.client.get(reverse('author-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_author(self):
        data = {"name": "George Orwell"}
        response = self.client.post(reverse('author-list'), data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_author(self):
        data = {"name": "Rowling, J.K."}
        response = self.client.put(reverse('author-detail', kwargs={'pk': self.author.pk}), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_author(self):
        response = self.client.delete(reverse('author-detail', kwargs={'pk': self.author.pk}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

class BookAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='testpass')
        self.client.login(username='testuser', password='testpass')
        self.author = Author.objects.create(name="J.K. Rowling")
        self.category = Category.objects.create(name="Fantasy")
        self.book = Book.objects.create(
            title="Harry Potter",
            author=self.author,
            published_date="2000-01-01",
            isbn_number="1234567890123",
            page_count=350,
            cover=None
        )
        self.book.categories.add(self.category)

    def test_list_books(self):
        response = self.client.get(reverse('book-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_book(self):
        data = {
            "title": "1984",
            "author": self.author.id,
            "published_date": "1949-06-08",
            "isbn_number": "1234567890124",
            "page_count": 328,
            "cover": None,
            "categories": [self.category.id]
        }
        response = self.client.post(reverse('book-list'), data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_book(self):
        data = {
            "title": "Harry Potter and the Philosopher's Stone",
            "author": self.author.id,
            "published_date": "2000-01-01",
            "isbn_number": "1234567890123",
            "page_count": 350,
            "cover": None,
            "categories": [self.category.id]
        }
        response = self.client.put(reverse('book-detail', kwargs={'pk': self.book.pk}), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_book(self):
        response = self.client.delete(reverse('book-detail', kwargs={'pk': self.book.pk}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

class CategoryAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='testpass')
        self.client.login(username='testuser', password='testpass')
        self.category = Category.objects.create(name="Fantasy")

    def test_list_categories(self):
        response = self.client.get(reverse('category-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_category(self):
        data = {"name": "Science Fiction"}
        response = self.client.post(reverse('category-list'), data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_category(self):
        data = {"name": "Sci-Fi"}
        response = self.client.put(reverse('category-detail', kwargs={'pk': self.category.pk}), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_category(self):
        response = self.client.delete(reverse('category-detail', kwargs={'pk': self.category.pk}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

