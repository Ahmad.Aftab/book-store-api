from django.urls import path
from . import views

urlpatterns = [
    # User registration and authentication
    path('register/', views.UserRegisterView.as_view(), name='register'),
    path('login/', views.CustomAuthToken.as_view(), name='login'),

    # CRUD for Author
    path('authors/', views.AuthorListCreateView.as_view(), name='author-list'),
    path('authors/<int:pk>/', views.AuthorDetailView.as_view(), name='author-detail'),

    # CRUD for Book
    path('books/', views.BookListCreateView.as_view(), name='book-list'),
    path('books/<int:pk>/', views.BookDetailView.as_view(), name='book-detail'),

    # CRUD for Category
    path('categories/', views.CategoryListCreateView.as_view(), name='category-list'),
    path('categories/<int:pk>/', views.CategoryDetailView.as_view(), name='category-detail'),

    # Shopping cart functionality
    path('cart/', views.ShoppingCartListCreateView.as_view(), name='cart-list'),
    path('cart/<int:pk>/', views.ShoppingCartDetailView.as_view(), name='cart-detail'),
    path('cart/add-book/<int:book_id>/', views.add_book_to_cart, name='add-book-to-cart'),
    path('cart/remove-book/<int:book_id>/', views.remove_book_from_cart, name='remove-book-from-cart'),
    path('cart/purchase/', views.purchase_cart, name='purchase-cart'),
]

