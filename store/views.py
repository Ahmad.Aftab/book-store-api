from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from django.contrib.auth.models import User
from .models import Book, Author, Category, ShoppingCart
from .serializers import UserSerializer, AuthorSerializer, BookSerializer, CategorySerializer, ShoppingCartSerializer
from .tasks import send_purchase_email

# User registration view
class UserRegisterView(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [AllowAny]

# Custom authentication token view to handle login and return a token.
class CustomAuthToken(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        token = Token.objects.get(key=response.data['token'])
        return Response({'token': token.key, 'id': token.user_id})

# CRUD views for Author, Book, Category, and ShoppingCart
class AuthorListCreateView(generics.ListCreateAPIView):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
    permission_classes = [IsAuthenticated]

class AuthorDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
    permission_classes = [IsAuthenticated]

class BookListCreateView(generics.ListCreateAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = [IsAuthenticated]

class BookDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = [IsAuthenticated]

class CategoryListCreateView(generics.ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = [IsAuthenticated]

class CategoryDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = [IsAuthenticated]

class ShoppingCartListCreateView(generics.ListCreateAPIView):
    queryset = ShoppingCart.objects.all()
    serializer_class = ShoppingCartSerializer
    permission_classes = [IsAuthenticated]

class ShoppingCartDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = ShoppingCart.objects.all()
    serializer_class = ShoppingCartSerializer
    permission_classes = [IsAuthenticated]

# Endpoints for the shopping cart functionality

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def add_book_to_cart(request, book_id):
    user = request.user
    book = get_object_or_404(Book, id=book_id)
    cart, _ = ShoppingCart.objects.get_or_create(user=user)
    cart.add_book(book)
    return Response({"message": "Book added to cart successfully."})

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def remove_book_from_cart(request, book_id):
    user = request.user
    book = get_object_or_404(Book, id=book_id)
    cart = get_object_or_404(ShoppingCart, user=user)
    cart.remove_book(book)
    return Response({"message": "Book removed from cart successfully."})

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def purchase_cart(request):
    user = request.user
    cart = get_object_or_404(ShoppingCart, user=user)
    total_price = cart.get_total_price()
    purchase_details = ", ".join([str(book) for book in cart.books.all()])  # Simplified example
    cart.books.clear()  # Empty the cart after the purchase
    send_purchase_email.delay(request.user.email, purchase_details)  # Queue the email task with Celery
    return Response({"message": f"Purchase successful. Total: ${total_price}"})
